//
//  FeedClient.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation
import Alamofire

protocol FeedClientType {
    func getFeedResponse(completion: @escaping (Result<NewsFeedResponse, AFError>)->Void)
}

/*
 The network client to fetch the feed data
 */
class FeedClient: FeedClientType {
    func getFeedResponse(completion: @escaping (Result<NewsFeedResponse, AFError>)->Void) {
        AF.request(Constants.Server.Endpoint.feed).responseDecodable(of: NewsFeedResponse.self) { response in
            completion(response.result)
        }
    }
}
