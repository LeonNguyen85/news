//
//  Article.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

struct ArticleEnclosure: Decodable {
    let link: String?
    let type: String?
    let thumbnail: String?
}

struct Article: Decodable {
    let title: String?
    let pubDate: String?
    let link: String?
    let guid: String?
    let author: String?
    let thumbnail: String?
    let description: String?
    let content: String?
    let enclosure: ArticleEnclosure?
    let categories:  [String]?

    var workingThumbnailUrlString: String {
        return workingUrlString(from: thumbnail)
    }

    var workingEnclosureUrlString: String {
        return workingUrlString(from: enclosure?.link)
    }

    private func workingUrlString(from urlString: String?) -> String {
        if let thumbnailString = urlString {
            let components = thumbnailString.components(separatedBy: String.questionMark)
            if components.count == 2 {
                return components[0]
            }
        }
        return .empty
    }
}
