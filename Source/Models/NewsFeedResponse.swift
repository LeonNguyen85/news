//
//  NewsFeedResponse.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

struct NewsFeedResponse: Decodable {
    let status: ResponseStatus
    let feed: Feed?
    let items: [Article]
}
