//
//  Feed.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

struct Feed: Decodable {
    let url: String?
    let title: String?
    let link: String?
    let author: String?
    let description: String?
    let image: String?
}
