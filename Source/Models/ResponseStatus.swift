//
//  ResponseStatus.swift
//  NewsTests
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

enum ResponseStatus: String, Decodable {
    case ok = "ok"
    case unknown
}
