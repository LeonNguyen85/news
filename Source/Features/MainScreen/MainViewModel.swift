//
//  MainViewModel.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

protocol MainViewModelType {
    var numberOfArticles: Int { get }
    var selectedIndexPath: IndexPath? { get set }
    func getFeedResponse(completion: @escaping (NewsFeedResponse?)->Void)
    func article(at index: Int) -> Article?
    func cellViewModel(at index: Int) -> MainViewTableCellModel?
    func isFeaturedCell(with index: IndexPath) -> Bool
}

/*
view model for main screen
 */
class MainViewModel: MainViewModelType {

    let feedClient: FeedClientType
    var feedResponse: NewsFeedResponse?
    var selectedIndexPath: IndexPath?

    var numberOfArticles: Int {
        return feedResponse?.items.count ?? 0
    }

    init(feedClient: FeedClientType = FeedClient()) {
        self.feedClient = feedClient
    }

    func article(at index: Int) -> Article? {
        if let response = feedResponse,
           response.items.count > index {
            return response.items[index]
        } else {
            return nil
        }
    }

    func isFeaturedCell(with index: IndexPath) -> Bool {
        index.section == 0 && index.row == 0
    }

    func cellViewModel(at index: Int) -> MainViewTableCellModel? {
        if let article = article(at: index) {
            return MainViewTableCellModel(article: article)
        }
        return nil
    }

    func getFeedResponse(completion: @escaping (NewsFeedResponse?)->Void) {
        feedClient.getFeedResponse { [weak self] (result) in
            if case .success(let feed) = result {
                self?.feedResponse = feed
                completion(feed)
            } else {
                completion(nil)
            }
        }
    }
}
