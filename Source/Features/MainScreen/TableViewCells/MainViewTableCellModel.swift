//
//  MainViewTableNormalCellModel.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit

protocol MainTableViewCellType {
    func setup(with viewModel: MainViewTableCellModel)
}
/*
cell view model on main screen
 */
class MainViewTableCellModel {
    let article: Article

    var title: String? {
        return article.title
    }

    var dateAttributedString: NSAttributedString? {
        guard let pubDateString = article.pubDate else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.responseDateTimeFormatString
        guard let dateTime = dateFormatter.date(from: pubDateString) else {
            return nil
        }
        dateFormatter.dateFormat = Constants.DateTimeFormat.displayDateFormatString
        let dateString = dateFormatter.string(from: dateTime)

        dateFormatter.dateFormat = Constants.DateTimeFormat.displayTimeFormatString
        let timeString = dateFormatter.string(from: dateTime)

        let attributedString: NSMutableAttributedString = .init(string: dateString + " ", attributes: semiboldAttributes)
        attributedString.append(NSAttributedString(string: timeString, attributes: normalAttributes))

        return attributedString
    }

    var workingThumbnailUrlString: String {
        article.workingThumbnailUrlString
    }

    var workingEnclosureUrlString: String {
        article.workingEnclosureUrlString
    }

    init(article: Article) {
        self.article = article
    }
}

fileprivate extension MainViewTableCellModel {
    var normalAttributes: [NSAttributedString.Key: Any] {
        return [.font: Constants.Font.regularStandard as Any,
                .foregroundColor: UIColor.gray]
    }

    var semiboldAttributes: [NSAttributedString.Key: Any] {
        return [.font: Constants.Font.boldStandardSmall as Any,
                .foregroundColor: UIColor.gray]
    }
}
