//
//  MainViewTableFeaturedCell.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit

/*
 Featured cell on main screen
 */
class MainViewTableFeaturedCell: UITableViewCell, MainTableViewCellType {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setup(with viewModel: MainViewTableCellModel) {
        titleLabel.text = viewModel.title
        titleLabel.font = Constants.Font.boldStandardLarge
        publishDateLabel.attributedText = viewModel.dateAttributedString
        mainImageView.downloadedFrom(link: viewModel.workingEnclosureUrlString, contentMode: .scaleAspectFill)
    }
}
