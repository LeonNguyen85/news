//
//  MainViewTableNormalCell.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit

/*
 Normal cell on main screen
 */
class MainViewTableNormalCell: UITableViewCell, MainTableViewCellType {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setup(with viewModel: MainViewTableCellModel) {
        titleLabel.text = viewModel.title
        titleLabel.font = Constants.Font.boldStandard
        publishDateLabel.attributedText = viewModel.dateAttributedString
        mainImageView.downloadThumbnailFrom(link: viewModel.workingThumbnailUrlString,
                                            size: CGSize(width: 150, height: 150),
                                            contentMode: .scaleAspectFill)
    }
}
