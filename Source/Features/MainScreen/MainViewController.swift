//
//  MainViewController.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import UIKit
import ProgressHUD

class MainViewController: UIViewController {

    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    private lazy var refreshControl = UIRefreshControl()

    var viewModel: MainViewModelType = MainViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()

        refresh(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let index = viewModel.selectedIndexPath {
            tableView.deselectRow(at: index, animated: true)
        }
    }

    private func setupTableView() {
        tableView.register(R.nib.mainViewTableNormalCell)
        tableView.register(R.nib.mainViewTableFeaturedCell)
        tableView.dataSource = self
        tableView.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: R.string.localizable.mainPullToRefresh(),
                                                            attributes: [.font: Constants.Font.regularStandard as Any,
                                                                         .foregroundColor: R.color.brandColor() as Any])
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }

    @objc func refresh(_ sender: AnyObject) {
        ProgressHUD.show()
        viewModel.getFeedResponse(completion: { [weak self] feed in
            DispatchQueue.main.async { [weak self] in
                self?.refreshControl.endRefreshing()
                if let feed = feed {
                    ProgressHUD.showSuccess()
                    let titleLabel = UILabel()
                    titleLabel.text = feed.feed?.title
                    titleLabel.textColor = R.color.brandColor()
                    titleLabel.font = Constants.Font.boldStandardLarge

                    self?.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: titleLabel)
                    self?.tableView.reloadData()
                } else {
                    ProgressHUD.showError()
                }
            }
        })
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfArticles
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if viewModel.isFeaturedCell(with: indexPath) {
            cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.mainViewTableFeaturedCell.identifier,
                                                 for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.mainViewTableNormalCell.identifier,
                                                 for: indexPath)
        }

        if let cell = cell as? MainTableViewCellType,
           let cellViewModel = viewModel.cellViewModel(at: indexPath.row) {
            cell.setup(with: cellViewModel)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedIndexPath = indexPath
        if let nextVC = R.storyboard.article.articleViewController(),
        let article = viewModel.article(at: indexPath.row) {
            let viewModel = ArticleViewModel(article: article)
            nextVC.viewModel = viewModel
            navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}

