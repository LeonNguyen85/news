//
//  ArticleViewModel.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit

protocol ArticleViewModelType {
    var article: Article { get }
    var workingEnclosureUrlString: String { get }
    var titleAttributedString: NSAttributedString? { get }
    var dateAttributedString: NSAttributedString? { get }
    var contentAttributedString: NSAttributedString? { get }
    var headingAttributedString: NSAttributedString? { get }
}

/*
view model for article screen
 */
class ArticleViewModel: ArticleViewModelType {

    let article: Article

    var headingAttributedString: NSAttributedString? {
        NSAttributedString(string: article.title ?? .empty,
                           attributes: [.font: Constants.Font.boldStandard as Any,
                                        .foregroundColor: R.color.brandColor() as Any])
    }

    var titleAttributedString: NSAttributedString? {
        NSAttributedString(string: article.title ?? .empty,
                           attributes: [.font: Constants.Font.boldStandardLarge as Any,
                                        .foregroundColor: R.color.brandColor() as Any])
    }

    var contentAttributedString: NSAttributedString? {
        NSAttributedString(string: article.content ?? .empty,
                           attributes: [.font: Constants.Font.boldStandardSmall as Any,
                                        .foregroundColor: UIColor.black])
    }

    var dateAttributedString: NSAttributedString? {
        guard let pubDateString = article.pubDate else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.responseDateTimeFormatString
        guard let dateTime = dateFormatter.date(from: pubDateString) else {
            return nil
        }
        dateFormatter.dateFormat = Constants.DateTimeFormat.displayDateFormatString
        let dateString = dateFormatter.string(from: dateTime)

        dateFormatter.dateFormat = Constants.DateTimeFormat.displayTimeFormatString
        let timeString = dateFormatter.string(from: dateTime)

        let attributedString: NSMutableAttributedString = .init(string: dateString + " ",
                                                                attributes: [.font: Constants.Font.boldStandardSmall as Any,
                                                                             .foregroundColor: UIColor.gray])
        attributedString.append(NSAttributedString(string: timeString,
                                                   attributes: [.font: Constants.Font.regularStandard as Any,
                                                                .foregroundColor: UIColor.gray]))

        return attributedString
    }

    var workingEnclosureUrlString: String {
        article.workingEnclosureUrlString
    }

    init(article: Article) {
        self.article = article
    }

}
