//
//  ArticleViewController.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!

    var viewModel: ArticleViewModelType?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = self.viewModel else {
            return
        }
        setupScreenTitle()
        mainImageView.downloadedFrom(link: viewModel.workingEnclosureUrlString, contentMode: .scaleAspectFill)
        titleLabel.attributedText = viewModel.headingAttributedString
        dateTimeLabel.attributedText = viewModel.dateAttributedString
        contentLabel.attributedText = viewModel.contentAttributedString
    }

    private func setupScreenTitle() {
        guard let viewModel = self.viewModel else {
            return
        }
        let navTitleLabel = UILabel()
        navTitleLabel.attributedText = viewModel.titleAttributedString
        navigationItem.titleView = navTitleLabel
    }
}
