//
//  AppProcessHelper.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

/*
Util to help with App Process
 */
public final class AppProcessHelper {

    private let processInfo: ProcessInfo

    /// The singleton instance in the system.
    public static let shared = AppProcessHelper()

    public var isTesting: Bool {
        #if DEBUG
        let env = processInfo.environment
        return env["XCTestConfigurationFilePath"] != nil || env["XCODE_DBG_XPC_EXCLUSIONS"] != nil
        #else
        return false
        #endif
    }

    private init(processInfo: ProcessInfo = ProcessInfo.processInfo) {
        self.processInfo = processInfo
    }
}
