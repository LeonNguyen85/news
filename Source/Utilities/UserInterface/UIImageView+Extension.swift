//
//  UIImageView+Extension.swift
//  News
//
//  Created by Leon Nguyen on 11/7/21.
//

import Foundation
import UIKit
import SDWebImage

/*
Util to download image async
 */
extension UIImageView {

    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill, placeholderImage: UIImage? = nil) {

        contentMode = mode
        sd_setImage(with: url, placeholderImage: placeholderImage)
    }

    func downloadThumbnailFrom(link: String,
                               size: CGSize,
                               contentMode mode: UIView.ContentMode = .scaleAspectFill,
                               placeholderImage: UIImage? = nil) {
        guard let url = URL(string: link) else { return }
        contentMode = mode

        let scale = UIScreen.main.scale
        let thumbnailSize = CGSize(width: size.width * scale, height: size.height * scale)
        sd_setImage(with: url, placeholderImage: placeholderImage, context: [.imageThumbnailPixelSize : thumbnailSize])
    }

    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill, placeholderImage: UIImage? = nil) {

        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode, placeholderImage: placeholderImage)
    }
}
