//
//  Debug.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

/*
Gloabl util function to print out debug info to console and Crashlytics
 */

#if canImport(Crashlytics)
import FirebaseCrashlytics
#endif

func dprint(_ items: Any..., file: String? = #file, line: Int? = #line) {
    #if DEBUG
        let info = mergeInfo(items: items, file: file, line: line)
        debugPrint(info)
    #endif
}

func log(_ items: Any..., file: String? = #file, line: Int? = #line) {
    let info = mergeInfo(items: items, file: file, line: line)
    #if canImport(Crashlytics)
    CLSLogv("%@", getVaList([info.description]))
    #endif
}

func exception(_ items: Any..., file: String? = #file, line: Int? = #line) {
    var info = mergeInfo(items: items, file: file, line: line)
    info.insert("Exception", at: 0)
    #if canImport(Crashlytics)
    CLSLogv("%@", getVaList([info.description]))
    #endif
}

fileprivate func mergeInfo(items: [Any], file: String?, line: Int?) -> [Any] {
    var items = items
    if let line = line {
        items.insert(line, at: 0)
    }
    if let filePath = file?.components(separatedBy: String.slash).last,
        let fileName = filePath.components(separatedBy: String.dot).first {
        items.insert(fileName, at: 0)
    }
    return items
}
