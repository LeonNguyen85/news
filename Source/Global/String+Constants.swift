//
//  String+Constants.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

/*
Extension to hold all the string constants
 */
public extension String {
    static var dot: String { "." }
    static var point: String { "•" }
    static var comma: String { "," }
    static var empty: String { "" }
    static var space: String { " " }
    static var slash: String { "/" }
    static var zero: String { "0" }
    static var dash: String { "-" }
    static var asterik: String { "*" }
    static var caret: String { "^" }
    static var dollar: String { "$" }
    static var lineBreak: String { "\n" }
    static var underscore: String { "_" }
    static var plus: String { "+" }
    static var one: String { "1" }
    static var letterO: String { "O" }
    static var letterI: String { "I" }
    static var pipe: String { "|" }
    static var at: String { "@" }
    static var questionMark: String { "?" }
}
