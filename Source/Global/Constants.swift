//
//  Constants.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation
import UIKit

/*
Struct to hold all the global constants
 */
struct Constants {
    struct Server {
        struct Endpoint {
            static let feed = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"
        }
    }
    struct Font {
        static let boldStandardLarge = UIFont(name: "TimesNewRomanPS-BoldMT", size: 22)
        static let boldStandard = UIFont(name: "TimesNewRomanPS-BoldMT", size: 17)
        static let boldStandardSmall = UIFont(name: "TimesNewRomanPS-BoldMT", size: 14)
        static let regularStandard = UIFont(name: "TimesNewRomanPSMT", size: 14)
    }
    struct DateTimeFormat {
        static let responseDateTimeFormatString = "yyyy-MM-dd HH:mm:ss"
        static let displayDateFormatString = "MMM dd, yyyy"
        static let displayTimeFormatString = "hh:mm a"
    }
}
