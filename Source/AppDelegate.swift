//
//  AppDelegate.swift
//  News
//
//  Created by Leon Nguyen on 10/7/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        #if DEBUG
        if AppProcessHelper.shared.isTesting {
            return true
        }
        #endif

        if let mainScreenVC = R.storyboard.main.mainViewController() {
            window = UIWindow(frame: UIScreen.main.bounds)
            let navigationController = UINavigationController(rootViewController: mainScreenVC)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }

        window?.backgroundColor = .black

        return true
    }
}

