//
//  MainViewTableNormalCellModelTests.swift
//  NewsTests
//
//  Created by Leon Nguyen on 11/7/21.
//

import Nimble
import Quick
@testable import News

final class MainViewTableNormalCellModelTests: QuickSpec {

    override func spec() {
        describe("test MainViewTableNormalCellModel") {
            context("with sample data") {
                it("should return correct thumbnail url") {

                    let article = Article(title: "Police say 'nobody scapegoated' in south-west Sydney COVID crackdown",
                                          pubDate: "",
                                          link: "",
                                          guid: "",
                                          author: "",
                                          thumbnail: "https://live-production.wcms.abc-cdn.net.au/0f687a0d0472b0bf53ae1f24724dbe9d?impolicy=wcms_crop_resize&amp;cropH=1080&amp;cropW=810&amp;xPos=555&amp;yPos=0&amp;width=862&amp;height=1149",
                                          description: "",
                                          content: "",
                                          enclosure: nil,
                                          categories: nil)
                    let viewModel = MainViewTableCellModel(article: article)

                    expect(viewModel.workingThumbnailUrlString) == "https://live-production.wcms.abc-cdn.net.au/0f687a0d0472b0bf53ae1f24724dbe9d"
                    expect(viewModel.title) == "Police say 'nobody scapegoated' in south-west Sydney COVID crackdown"
                }
            }
        }
    }
}
