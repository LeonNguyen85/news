//
//  NewsTests.swift
//  NewsTests
//
//  Created by Leon Nguyen on 10/7/21.
//

import Nimble
import Quick
@testable import News

final class NewsFeedResponseTests: QuickSpec {

    override func spec() {
        describe("decode NewsFeedResponseData") {
            context("with sample data") {
                it("should return correct object") {
                    let data = Bundle.specs.jsonData(fromFile: "NewsFeedResponseData") ?? Data()
                    let result = try! JSONDecoder().decode(NewsFeedResponse.self, from: data)
                    expect(result).toNot(beNil())
                    expect(result.status) == .ok

                    let feed = result.feed
                    expect(feed).toNot(beNil())
                    expect(feed?.author) == .empty
                    expect(feed?.description) == .empty
                    expect(feed?.image) == "https://www.abc.net.au/news/image/8413416-1x1-144x144.png"
                    expect(feed?.link) == "https://www.abc.net.au/news/justin/"
                    expect(feed?.title) == "Just In"
                    expect(feed?.url) == "http://www.abc.net.au/news/feed/51120/rss.xml"

                    expect(result.items.count) == 10
                    let article = result.items[0]
                    expect(article.title) == "Scott Morrison backs Melbourne pub's offer of a free beer for vaccinated patrons"
                    expect(article.pubDate) == "2021-07-09 00:51:56"
                    expect(article.link) == "https://www.abc.net.au/news/2021-07-09/prime-minister-scott-morrison-backs-covid-vaccine-promotion-beer/100280146"
                    expect(article.guid) == "https://www.abc.net.au/news/2021-07-09/prime-minister-scott-morrison-backs-covid-vaccine-promotion-beer/100280146"
                    expect(article.author) == .empty
                    expect(article.thumbnail) == "https://live-production.wcms.abc-cdn.net.au/aa786a36e77d00c8a89dc62cd46e7357?impolicy=wcms_crop_resize&amp;cropH=1997&amp;cropW=1500&amp;xPos=1191&amp;yPos=8&amp;width=862&amp;height=1149"
                    expect(article.description) == "Prime Minister Scott Morrison backs a Melbourne pub's offer of a free pint to vaccinated patrons, after the Therapeutic Goods Administration told the publican to pull the campaign."
                    expect(article.content) == "Prime Minister Scott Morrison backs a Melbourne pub's offer of a free pint to vaccinated patrons, after the Therapeutic Goods Administration told the publican to pull the campaign."

                    let enclosure = article.enclosure
                    expect(enclosure).toNot(beNil())
                    expect(enclosure?.link) == "https://live-production.wcms.abc-cdn.net.au/aa786a36e77d00c8a89dc62cd46e7357?impolicy=wcms_crop_resize&amp;cropH=1680&amp;cropW=2982&amp;xPos=0&amp;yPos=308&amp;width=862&amp;height=485"
                    expect(enclosure?.type) == "image/jpeg"
                    expect(enclosure?.thumbnail) == "https://live-production.wcms.abc-cdn.net.au/aa786a36e77d00c8a89dc62cd46e7357?impolicy=wcms_crop_resize&amp;cropH=1997&amp;cropW=1500&amp;xPos=1191&amp;yPos=8&amp;width=862&amp;height=1149"

                    let categories = article.categories
                    expect(categories?.count) == 3
                    expect(categories?[0]) == "COVID-19"
                }
            }
        }
    }
}
