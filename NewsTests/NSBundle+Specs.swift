//
//  NSBundle+Specs.swift
//  NewsTests
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

extension Bundle {

    static let specs = Bundle(for: SpecsBundle.self)
}

private class SpecsBundle {}
