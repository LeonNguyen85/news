//
//  ArticleTests.swift
//  NewsTests
//
//  Created by Leon Nguyen on 11/7/21.
//

import Nimble
import Quick
@testable import News

final class ArticleTests: QuickSpec {

    override func spec() {
        describe("test Article") {
            context("with sample data") {
                it("should return correct url strings") {

                    let enclosure = ArticleEnclosure(link: "https://live-production.wcms.abc-cdn.net.au/aa786a36e77d00c8a89dc62cd46e7357?impolicy=wcms_crop_resize&amp;cropH=1680&amp;cropW=2982&amp;xPos=0&amp;yPos=308&amp;width=862&amp;height=485",
                                                     type: nil,
                                                     thumbnail: nil)
                    let article = Article(title: "Police say 'nobody scapegoated' in south-west Sydney COVID crackdown",
                                          pubDate: "",
                                          link: "",
                                          guid: "",
                                          author: "",
                                          thumbnail: "https://live-production.wcms.abc-cdn.net.au/0f687a0d0472b0bf53ae1f24724dbe9d?impolicy=wcms_crop_resize&amp;cropH=1080&amp;cropW=810&amp;xPos=555&amp;yPos=0&amp;width=862&amp;height=1149",
                                          description: "",
                                          content: "",
                                          enclosure: enclosure,
                                          categories: nil)

                    expect(article.workingThumbnailUrlString) == "https://live-production.wcms.abc-cdn.net.au/0f687a0d0472b0bf53ae1f24724dbe9d"
                    expect(article.workingEnclosureUrlString) == "https://live-production.wcms.abc-cdn.net.au/aa786a36e77d00c8a89dc62cd46e7357"
                }
            }
        }
    }
}
