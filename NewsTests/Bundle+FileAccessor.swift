//
//  Bundle+FileAccessor.swift
//  NewsTests
//
//  Created by Leon Nguyen on 10/7/21.
//

import Foundation

extension Bundle {

    public func data(fromFile name: String, ofType type: String? = nil) -> Data? {
        return path(forResource: name, ofType: type)
            .map { URL(fileURLWithPath: $0) }
            .flatMap { try? Data(contentsOf: $0) }
    }

    public func json(fromFile name: String) -> Any? {
        return jsonData(fromFile: name)
            .flatMap { try? JSONSerialization.jsonObject(with: $0, options: []) }
    }

    public func jsonData(fromFile name: String) -> Data? {
        return data(fromFile: name, ofType: "json")
    }
}
