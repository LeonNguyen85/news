# Design considerations and decisions
## Decisions
 - Choose R.swift library for convenient asset referencing
 - Choose Alamofire for an easy start, in the long run, having our own network library has a lot of benefits (i.e: better error handling, better security handling)
 - Choose MVVM as the architecture of the app
 - Choose ProgressHUD library to display the loading indicator, this library offers many variation of the spinner and it’s quick to implement
 - The original image url is broken so fix it by eliminating the query string
 - Sorting project files using Apple script to avoid code conflicts in project files
## Future considerations
 - Consider Crashlytics for Crash Reporting
 - Consider using RealmDB for data caching
 - Consider using Eureka library for table view
 - Consider testing `var dateAttributedString: NSAttributedString`
